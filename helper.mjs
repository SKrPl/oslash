import { readFileSync } from 'fs';

import fastifySwagger from 'fastify-swagger';
import fastifyElasticSearch from 'fastify-elasticsearch';
import fsequelize from 'sequelize-fastify';

import shortcutRoutes from './api/routes/shortcuts.mjs';
import { userUnauthenticatedRoutes, signOutRoute }from './api/routes/users.mjs';
import userModel from './api/models/user.mjs';
import expiredTokenModel from './api/models/expiredToken.mjs';

const shortcutMappig = JSON.parse(readFileSync('./api/models/mappings/shortcut.json', { encoding: 'utf-8' }));

/**
 * @typedef {Object} PluginConfigs
 * @property {Object} plugin Fastify plugin
 * @property {Object} options Fastify plugin options
 */

/**
 * Register fastify plugins to the fastify instance.
 *
 * @param {Fastify} fastify Fastify server instance
 * @param {Array.<PluginConfigs>} pluginConfigs Array of plugins to be registered along with their options.
 */
export function registerPlugins (fastify, pluginConfigs = []) {
  for (const { plugin, options } of pluginConfigs) {
    fastify.register(plugin, options);
  }
}

/**
 * Start a fastify server.
 *
 * @param {Fastify} fastify Fastify server instance
 * @param {Number} port Port number for starting server
 */
export async function start (fastify, port) {
  try {
    await fastify.listen(port);
    await fastify.sequelize.sync();

    const { body: shortcutIndexExists } = await fastify.elastic.indices.exists({
      index: 'shortcuts'
    });

    if (!shortcutIndexExists) {
      await fastify.elastic.indices.create({
        index: 'shortcuts',
        body: {
          mappings: shortcutMappig
        }
      });
    }
  } catch (e) {
    fastify.log.error(e);
    process.exit(1);
  }
}

// TODO: use dot-env equivalent JS library
export const JWT_SECRET = 'jwtSecret';

export const pluginConfigs = [
  {
    plugin: fastifySwagger,
    options: {
      exposeRoute: true,
      routePrefix: '/docs',
      info: {
        title: 'OSlash Backend'
      }
    }
  },
  {
    plugin: fsequelize,
    options: {
      instance: 'sequelize',
      sequelizeOptions: {
        dialect: 'mysql',
        database: 'oslash',
        username: 'root',
        password: 'root',
        logging: false
      }
    }
  },
  {
    plugin: fastifyElasticSearch,
    options: {
      node: 'http://localhost:9200'
    }
  },
  {
    plugin: shortcutRoutes
  },
  {
    plugin: userUnauthenticatedRoutes
  },
  {
    plugin: signOutRoute
  },
  {
    plugin: userModel
  },
  {
    plugin: expiredTokenModel
  }
];

