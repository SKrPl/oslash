import Fastify from 'fastify';

import { pluginConfigs, registerPlugins, start } from './helper.mjs';

const PORT = 5000;
let fastify = Fastify({ logger: true });

registerPlugins(fastify, pluginConfigs);
start(fastify, PORT);
