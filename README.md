# OSlash

## API Selection
REST API is used to implement the barebones OSlash backend app.

As the required endpoints are implemented in a single service, the client will send request to
a single service. So, GraphQL is not required. GraphQL would have been helpful, if there are
multiple user facing services and clients have to take the heavy loading of fetching all the data.

gRPC is usually used for inter microservice communication due to its multi-language support and
performant serialization and de-serialization of payloads. So, gRPC is not used as it's a single
service.

## Authentication Mechanism
JWT is used to authenticate users.

JWT is easy to implement and is stateless. Also, as the app is a bare minimum implementation, it is not
required to verify users using their Google, Github, Twitter, Apple etc. accounts. Ideally, OAuth2
implementaton should be preferred for a full featured implementaton.

## Database
Elastic search (ES) is used to store the shortcuts and MySQL 8 is used to store user details.

The main reason for selecting elastic search is the requirement to have fuzzy search feature for
the created shortcuts. Compared to other databases elastic search is well established for this
usecase.

### Why NoSQL?
1. As the product matures, there will be a lot of users, and the amount of shortcuts created will be
huge. So, it is better to opt for a DB which can be scaled horizontally.
2. Adding new attributes for the shortcut object is easier in NoSQL DBs. In relational DBs it may 
require complex DB migrations.


### Ideal Solution
If we go by CAP theorem, ES provides availability and partition tolerance. It doesn't provide
consistency. Instead of directly ingesting data into ES, the data should be injested into a
NoSQL DB (e.g. Mongo DB) which provides consistency and partition tolerance and then streamed
into ES.

For only searching of shortcuts, ES should be used directly.

## DB Design

### User table
Stores details of registered users.

```SQL
CREATE TABLE `user` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username__password` (`username`,`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
```

### Expired token table
Used for implementing signout feature, to store non-expired JWT tokens. If using these non-expired
token a user tries to perform an operation, the response is 401.

```SQL
CREATE TABLE `expired_token` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `userId` int unsigned NOT NULL,
  `token` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userId__token` (`userId`,`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
```

### Shortcut static mapping of Elastic Search
Please refer to [shortcut mapping](api/models/mappings/shortcut.json) file.

## Development Environment setup
1. Clone the repo and `npm install` to install NodeJS dependencies.
2. Install MySQL 8 and Elastic Search (execute the command in Single Node ES section).
3. Execute `CREATE DATABASE oslash;` after logging into MySQL 8.

### Commands
1. `npm run test` to execute all tests.
2. `npm run start` to start the server.
3. `npm run dev` to start the server for developement purpose.

### Single Node ES
**Dependency: Docker**

```bash
docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" --memory 4096m --net elastic --name elastic-search docker.elastic.co/elasticsearch/elasticsearch:7.16.1
```

If you wish to setup ES without docker please refer to [installation docs](https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html).

## API Documentation
OpenAPI specification of the endpoints can be found at [/docs](http://localhost:5000/docs)
after the server has been started.

## TODO
1. Use dot-env for configuration
2. [WIP]Use docker compose for elastic-search and mysql setup.
