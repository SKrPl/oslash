import { DataTypes } from 'sequelize';

export default async function ExpiredToken (fastify, options) {
  fastify.sequelize.define('ExpiredToken', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      primaryKey: true,
      autoIncrement: true
    },
    userId: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
    },
    token: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  }, {
    tableName: 'expired_token',
    indexes: [
      {
        name: 'userId__token',
        fields: [
          'userId',
          'token'
        ],
        unique: true
      }
    ]
  });
};
