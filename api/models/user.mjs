import { DataTypes } from 'sequelize';

export default async function User (fastify, options) {
  fastify.sequelize.define('User', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    username: {
      type: DataTypes.STRING(128),
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  }, {
    tableName: 'user',
    indexes: [
      {
        name: 'username__password',
        fields: [
          'username',
          'password'
        ],
        unique: true
      }
    ]
  });
};
