import jwt from 'jsonwebtoken';

import { JWT_SECRET } from '../../helper.mjs';

/**
 * Create JSON Web Token.
 *
 * @param {Object} payload JSON object to be encoded.
 * @param {String} secret Secret key for signing the payload
 * @param {String | Number} expiryTime Time after which the token would be expired
 * @returns {String} JWT
 */
export function sign (payload, secret, expiryTime) {
  return jwt.sign(payload, secret, { expiresIn: expiryTime });
}

/**
 * Verify if a JWT is signed or not.
 *
 * @param {String} token JWT to be verified
 * @param {String} secret Secret that will be used for verification
 * @returns {Object | String} decoded value
 */
export function verify (token, secret) {
  return jwt.verify(token, secret);
}


/**
 * Verify access token for a request.
 * 
 * @param {String} key Access token
 * @param {Object} request Fastify request object
 */
export function auth (key, request) {
  try {
    const decodedPayload = verify(key, JWT_SECRET);

    request.user = {
      username: decodedPayload.username,
      id: decodedPayload.id
    };

    return true;
  }
  catch (error) {
    if (error instanceof jwt.JsonWebTokenError) {
      return false;
    }
  }

  return 'error'; // results in 500 status code
}

export async function checkExpiredToken (sequelize, userId, token) {
  const { ExpiredToken } = sequelize.models;
  const isTokenExpired = await ExpiredToken.findOne({
    where: {
      userId,
      token
    }
  });

  if (isTokenExpired) {
    return true;
  }

  return false;
}
