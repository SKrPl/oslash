import bearerAuthPlugin from 'fastify-bearer-auth';

import { createUser, signin, signout } from '../controllers/users.mjs';
import { auth } from '../utils/authentication.mjs';

const validCredentialResponseSchema = {
  type: 'object',
  properties: {
    id: {
      type: 'number'
    },
    token: {
      type: 'string'
    }
  }
};

const signupOptions = {
  schema: {
    body: {
      type: 'object',
      required: [
        'name',
        'username',
        'password'
      ],
      properties: {
        name: {
          type: 'string',
          minLength: 4,
          maxLength: 255
        },
        username: {
          type: 'string',
          minLength: 4,
          maxLength: 126
        },
        password: {
          type: 'string',
          minLength: 6,
          maxLength: 20
        }
      }
    },
    response: {
      201: validCredentialResponseSchema
    }
  },
  handler: createUser
};

const signinOptions = {
  schema: {
    body: {
      type: 'object',
      required: [
        'username',
        'password'
      ],
      properties: {
        username: {
          type: 'string',
          minLength: 4,
          maxLength: 126
        },
        password: {
          type: 'string',
          minLength: 6,
          maxLength: 20
        }
      }
    },
  },
  response: {
    200: validCredentialResponseSchema
  },
  handler: signin
};

const signoutOptions = {
  schema: {
    headers: {
      type: 'object',
      properties: {
        authroization: {
          type: 'string'
        }
      },
    },
    response: {
      205: {
        type: 'null'
      }
    }
  },
  handler: signout
};

export async function userUnauthenticatedRoutes (fastify, options) {
  // user-signup
  fastify.post('/signup', signupOptions);

  // user signin
  fastify.post('/signin', signinOptions);
};

export async function signOutRoute (fastify, options) {
  fastify.register(bearerAuthPlugin, {
    bearerType: 'Bearer',
    auth: auth,
    addHook: true
  });

  fastify.post('/signout', signoutOptions);
}
