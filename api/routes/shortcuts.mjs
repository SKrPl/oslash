import bearerAuthPlugin from 'fastify-bearer-auth';

import { createShorcut, deleteShortcut, findShortcuts } from '../controllers/shortcuts.mjs';
import { auth } from '../utils/authentication.mjs';

const shortcutSchema = {
  type: 'object',
  required: [
    'shortlink',
    'url'
  ],
  properties: {
    shortlink: {
      type: 'string'
    },
    description: {
      type: 'string'
    },
    url: {
      type: 'string',
      format: 'uri'
    },
    tags: {
      type: 'array',
      items: {
        type: 'string'
      }
    }
  }
};

const createShortcutOptions = {
  schema: {
    body: shortcutSchema,
    headers: {
      type: 'object',
      properties: {
        authroization: {
          type: 'string'
        }
      },
    },
    response: {
      201: {
        type: 'object',
        properties: {
          id: {
            type: 'string'
          }
        }
      }
    }
  },
  handler: createShorcut
};

const findShortcutsOptions = {
  schema: {
    querystring: {
      type: 'object',
      properties: {
        query: {
          type: 'string'
        }
      }
    },
    headers: {
      type: 'object',
      properties: {
        authroization: {
          type: 'string'
        }
      },
    },
    response: {
      200: {
        type: 'object',
        required: [
          'shortcuts',
        ],
        properties: {
          shortcuts: {
            type: 'array',
            items: shortcutSchema
          }
        }
      }
    }
  },
  handler: findShortcuts
};

const deleteShortcutOptions = {
  schema: {
    headers: {
      type: 'object',
      properties: {
        authroization: {
          type: 'string'
        }
      },
    },
    response: {
      200: {
        type: 'object',
        required: [
          'id'
        ],
        properties: {
          id: {
            type: 'string'
          }
        }
      }
    }
  },
  handler: deleteShortcut
};

export default async function shortcutRoutes (fastify, options) {
  fastify.register(bearerAuthPlugin, {
    bearerType: 'Bearer',
    auth: auth,
    addHook: true
  });

  // create a shortcut
  fastify.post('/shortcuts', createShortcutOptions);

  // get all shortcuts
  fastify.get('/shortcuts', findShortcutsOptions);

  // delete a shortcut
  fastify.delete('/shortcuts/:id', deleteShortcutOptions);
};
