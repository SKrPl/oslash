import bcrypt from 'bcryptjs';

import { sign } from '../utils/authentication.mjs';
import { JWT_SECRET } from '../../helper.mjs';
import { checkExpiredToken } from '../utils/authentication.mjs';

const USER_ALREADY_EXISTS_ERROR_RESPONSE = { error: 'user already exists' };
const SERVER_ERROR_RESPONSE = { error: 'server error' };
const UNAUTHORIZED_ERROR_RESPONSE = { error: 'unauthorized error' };

export async function createUser (request, reply) {
  const { name, username, password } = request.body;
  const { User } = this.sequelize.models;

  try {
    const existingUsers = await User.findOne({
      where: { username }
    });

    if (existingUsers) {
      reply.status(409);
      return USER_ALREADY_EXISTS_ERROR_RESPONSE;
    }
  }
  catch (dbError) {
    this.log.error(dbError);
    reply.status(500);

    return SERVER_ERROR_RESPONSE;
  }

  const hashedPassword = await bcrypt.hash(password, 10);
  let userDetails;

  try {
    userDetails = await User.create({
      name: name,
      username: username,
      password: hashedPassword
    });
  }
  catch (dbError) {
    this.log.error(dbError);

    reply.status(500);

    return SERVER_ERROR_RESPONSE;
  }

  reply.status(201);

  return {
    id: userDetails.id,
    token: sign({ id: userDetails.id, username: username }, JWT_SECRET, '1h')
  };
}

export async function signin (request, reply) {
  const { username, password } = request.body;
  const { User } = this.sequelize.models;

  let existingUser;

  try {
    existingUser = await User.findOne({
      where: { username }
    });

    if (!existingUser) {
      reply.status(401);

      return { error: 'user is not registered' };
    }
  }
  catch (dbError) {
    this.log.error(dbError);
    reply.status(500);

    return SERVER_ERROR_RESPONSE;
  }

  const hasCorrectPassword = await bcrypt.compare(password, existingUser.password);

  if (!hasCorrectPassword) {
    reply.status(401);

    return { error: 'invalid credentials' };
  }

  return {
    id: existingUser.id,
    token: sign({ id: existingUser.id, username: username }, JWT_SECRET, '1h')
  };
}

export async function signout (request, reply) {
  const { ExpiredToken } = this.sequelize.models;
  const { id: userId } = request.user;
  const token = request.headers.authorization.split(' ')[1];

  let isTokenExpired;

  try {
    isTokenExpired = await checkExpiredToken(fastify.sequelize, userId, token);
  }
  catch (error) {
    this.log.error(error);
    reply.status(500);

    return SERVER_ERROR_RESPONSE;
  }

  if (isTokenExpired) {
    reply.status(401);

    return UNAUTHORIZED_ERROR_RESPONSE;
  }

  try {
    await ExpiredToken.create({
      userId,
      token
    });
  }
  catch (dbError) {
    reply.status(500);

    return SERVER_ERROR_RESPONSE;
  }

  await reply.status(205).send();
}
