import lodash from 'lodash';

import { checkExpiredToken } from '../utils/authentication.mjs';

const { get, set } = lodash;
const ES_INDEX = 'shortcuts';
const SERVER_ERROR_RESPONSE = { error: 'server error' };
const FORBIDDEN_ERROR_RESPONSE = { error: 'forbidden error' };
const UNAUTHORIZED_ERROR_RESPONSE = { error: 'unauthorized error' };

// TOOD: move expired token check logic to a middleware

export async function createShorcut (request, reply) {
  const shortLinkDetails = request.body;
  const { id: userId } = request.user;
  const token = request.headers.authorization.split(' ')[1];

  let shortcutId;
  let isTokenExpired;

  try {
    isTokenExpired = await checkExpiredToken(fastify.sequelize, userId, token);
  }
  catch (error) {
    this.log.error(error);
    reply.status(500);

    return SERVER_ERROR_RESPONSE;
  }

  if (isTokenExpired) {
    reply.status(401);

    return UNAUTHORIZED_ERROR_RESPONSE;
  }

  try {
    const response = await this.elastic.index({
      index: ES_INDEX,
      body: { ...shortLinkDetails, userId }
    });

    shortcutId = response.body._id;
    reply.status(201);
  } catch (error) {
    this.log.error(error);

    reply.status(500);
  }

  const response = reply.statusCode >= 500 ? SERVER_ERROR_RESPONSE : { id: shortcutId };

  return response;
}

export async function findShortcuts (request, reply) {
  const { id: userId } = request.user;
  const { query } = request.query;
  const token = request.headers.authorization.split(' ')[1];

  let isTokenExpired;

  try {
    isTokenExpired = await checkExpiredToken(fastify.sequelize, userId, token);
  }
  catch (error) {
    this.log.error(error);
    reply.status(500);

    return SERVER_ERROR_RESPONSE;
  }

  if (isTokenExpired) {
    reply.status(401);

    return UNAUTHORIZED_ERROR_RESPONSE;
  }

  const searchCriteria = {
    query: {
      bool: {
        filter: [
          {
            term: {
              userId
            }
          }
        ]
      }
    }
  };

  if (query) {
    set(searchCriteria,
      [
        'query',
        'bool',
        'must',
        'multi_match'
      ],
      {
        query: query,
        fields: [
          'shortlink',
          'description',
          'tags'
        ]
      });
  }

  let shortcuts;

  try {
    const response = await this.elastic.search({
      index: ES_INDEX,
      body: searchCriteria
    });

    shortcuts = get(response, 'body.hits.hits', []).map((hit) => {
      return hit._source;
    });

    reply.status(200);
  }
  catch (error) {
    this.log.error(error);

    reply.status(500);
  }

  return reply.statusCode >= 500 ? SERVER_ERROR_RESPONSE : { shortcuts };
}

export async function deleteShortcut (request, reply) {
  const shortcutId = request.params.id;
  const { id: userId } = request.user;
  const token = request.headers.authorization.split(' ')[1];

  let isTokenExpired;

  try {
    isTokenExpired = await checkExpiredToken(fastify.sequelize, userId, token);
  }
  catch (error) {
    this.log.error(error);
    reply.status(500);

    return SERVER_ERROR_RESPONSE;
  }

  if (isTokenExpired) {
    reply.status(401);

    return UNAUTHORIZED_ERROR_RESPONSE;
  }

  let response;

  // TODO: optimize it using deleteByQuery API
  try {
    const dbResponse = await this.elastic.get({
      index: ES_INDEX,
      id: shortcutId
    });
    const shortcut = get(dbResponse, 'body._source', null);

    if (shortcut.userId !== userId) {
      reply.status(403);
      response = FORBIDDEN_ERROR_RESPONSE;
    }
  }
  catch (error) {
    const statusCode = error.statusCode >= 500 ? 500 : 403;
    response = statusCode === 403 ? FORBIDDEN_ERROR_RESPONSE : SERVER_ERROR_RESPONSE;
    
    reply.status(statusCode);
    
    if (statusCode >= 500) {
      this.log.error(error);
    }
  }

  if (reply.statusCode >= 400) {
    return response;
  }


  try {
    await this.elastic.delete({
      index: ES_INDEX,
      id: shortcutId
    });

    reply.status(200);
    response = { id: shortcutId };
  }
  catch (error) {
    this.log.error(error);
    reply.response(500);
    response = SERVER_ERROR_RESPONSE
  }

  return response;
}
