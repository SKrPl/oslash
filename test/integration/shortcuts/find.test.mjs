import { expect } from 'chai';

import { clearDataStore } from '../../bootstrap.mjs';

describe('find shortcuts controller action', function () {
  let accessToken;
  let accessToken2;

  beforeEach(async function () {
    const userResponse1 = await fastify.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        name: 'Siddhant Kumar Patel',
        username: 'oslash',
        password: 'hi-there-abcd'
      }
    });
    const userReponse2 = await fastify.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        name: 'Sergio Busquets',
        username: 'busquets',
        password: 'hi-there-abcd'
      }
    });

    accessToken = userResponse1.json().token;
    accessToken2 = userReponse2.json().token;
  });

  afterEach(async function() {
    await clearDataStore(fastify.elastic, fastify.sequelize);
  });

  it('should relay unauthorized error if access token is not provided', async function () {
    const res = await fastify.inject({
      method: 'GET',
      url: '/shortcuts',
    });

    expect(res.statusCode).to.equal(401);
  });

  it('should return zero shortcuts for a user who has not created any shortcuts', async function () {
    const res = await fastify.inject({
      method: 'GET',
      url: '/shortcuts',
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });

    const response = res.json();

    expect(res.statusCode).to.equal(200);
    expect(response.shortcuts).to.be.an('array');
    expect(response.shortcuts).to.have.lengthOf(0);
  });

  it('should return shortcuts of a user', async function () {
    const payloadAndHeaders = [
      {
        payload: {
          shortlink: 'standup',
          description: 'Google meet standup link',
          url: 'https://meet.google.com/abc-123-xyz',
          tags: [
            "standup",
            "meeting",
            "google"
          ]
        },
        headers: {
          authorization: `Bearer ${accessToken}`
        }
      },
      {
        payload: {
          shortlink: 'payroll',
          description: 'My Payroll',
          url: 'https://payroll.com/me',
          tags: [
            "money",
            "payroll"
          ]
        },
        headers: {
          authorization: `Bearer ${accessToken}`
        }
      },
      {
        payload: {
          shortlink: 'standup',
          description: 'Zoom standup link',
          url: 'https://zoom.com/abc-123-xyz',
          tags: [
            "standup",
            "meeting",
            "zoom"
          ]
        },
        headers: {
          authorization: `Bearer ${accessToken2}`
        }
      }
    ];
    const shortcutPromises = payloadAndHeaders.map(({payload, headers}) => {
      return fastify.inject({
        url: '/shortcuts',
        method: 'POST',
        payload: payload,
        headers: headers
      });
    });

    await Promise.all(shortcutPromises);
    await fastify.elastic.indices.refresh({
      index: '_all'
    });

    const res = await fastify.inject({
      method: 'GET',
      url: '/shortcuts',
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });
    const response = res.json();

    expect(res.statusCode).to.equal(200);
    expect(response.shortcuts).to.have.deep.members([
      {
        shortlink: 'standup',
        description: 'Google meet standup link',
        url: 'https://meet.google.com/abc-123-xyz',
        tags: [
          "standup",
          "meeting",
          "google"
        ]
      },
      {
        shortlink: 'payroll',
        description: 'My Payroll',
        url: 'https://payroll.com/me',
        tags: [
          "money",
          "payroll"
        ]
      }
    ]);
  });

  describe('search', function () {
    it('should return a matching shortcuts for a user', async function () {
      const payloadAndHeaders = [
        {
          payload: {
            shortlink: 'standup',
            description: 'Google meet standup link',
            url: 'https://meet.google.com/abc-123-xyz',
            tags: [
              "standup",
              "meeting",
              "google"
            ]
          },
          headers: {
            authorization: `Bearer ${accessToken}`
          }
        },
        {
          payload: {
            shortlink: 'payroll',
            description: 'My Payroll',
            url: 'https://payroll.com/me',
            tags: [
              "money",
              "payroll"
            ]
          },
          headers: {
            authorization: `Bearer ${accessToken}`
          }
        },
        {
          payload: {
            shortlink: 'standup',
            description: 'Zoom standup link',
            url: 'https://zoom.com/abc-123-xyz',
            tags: [
              "standup",
              "meeting",
              "zoom"
            ]
          },
          headers: {
            authorization: `Bearer ${accessToken2}`
          }
        }
      ];

      const shortcutPromises = payloadAndHeaders.map(({payload, headers}) => {
        return fastify.inject({
          url: '/shortcuts',
          method: 'POST',
          payload: payload,
          headers: headers
        });
      });

      await Promise.all(shortcutPromises);
      await fastify.elastic.indices.refresh({
        index: '_all'
      });

      const res = await fastify.inject({
        method: 'GET',
        url: '/shortcuts',
        query: {
          query: 'meeting'
        },
        headers: {
          authorization: `Bearer ${accessToken}`
        }
      });
      const response = res.json();

      expect(res.statusCode).to.equal(200);
      expect(response.shortcuts).to.have.deep.members([
        {
          shortlink: 'standup',
          description: 'Google meet standup link',
          url: 'https://meet.google.com/abc-123-xyz',
          tags: [
            "standup",
            "meeting",
            "google"
          ]
        },
      ]);
    });
  });
});
