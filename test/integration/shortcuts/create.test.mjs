import { expect } from 'chai';

import { clearDataStore } from '../../bootstrap.mjs';

describe('create shortcuts controller action', function () {
  let accessToken;

  beforeEach(async function () {
    const res = await fastify.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        name: 'Siddhant Kumar Patel',
        username: 'oslash',
        password: 'hi-there-abcd'
      }
    });

    accessToken = res.json().token;
  });

  afterEach(async function () {
    await clearDataStore(fastify.elastic, fastify.sequelize);
  });

  it('should relay unauthorized error if authorization header is not sent', async function () {
    const res = await fastify.inject({
      method: 'POST',
      url: '/shortcuts',
      payload: {
        shortlink: 'hello'
      }
    });

    expect(res.statusCode).to.equal(401);
  });

  it('should relay unauthorized error if incorrect access token is sent', async function () {
    const payload = {
      shortlink: 'Hello',
      description: 1234,
      url: ['hi there'],
      tags: [
        "standup",
        "meeting",
        "google"
      ]
    };
    const res = await fastify.inject({
      url: '/shortcuts',
      method: 'POST',
      payload: payload,
      headers: {
        authorization: 'Bearer: abcd-1234'
      }
    });

    expect(res.statusCode).to.equal(401);
  });

  it('should relay bad request error if incorrect payload is sent', async function () {
    const payload = {
      shortlink: 'Hello',
      description: 1234,
      url: ['hi there'],
      tags: [
        "standup",
        "meeting",
        "google"
      ]
    };
    const res = await fastify.inject({
      url: '/shortcuts',
      method: 'POST',
      payload: payload,
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });

    expect(res.statusCode).to.equal(400);
  });

  it('should create a shortcut', async function () {
    const payload = {
      shortlink: 'standup',
      description: 'Google meet standup link',
      url: 'https://meet.google.com/abc-123-xyz',
      tags: [
        "standup",
        "meeting",
        "google"
      ]
    };
    const res = await fastify.inject({
      url: '/shortcuts',
      method: 'POST',
      payload: payload,
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });
    const response = res.json();
    const shortcutId = response.id;

    expect(res.statusCode).to.equal(201);
    expect(shortcutId).to.be.ok;

    // check if the document is stored in the data store
    const { body: responseBody } = await fastify.elastic.get({
      index: 'shortcuts',
      id: shortcutId
    });

    expect(responseBody._source).to.eql({ ...payload, userId: 1 });
  });
});
