import chai, { expect } from 'chai';
import chaiAsPromised from 'chai-as-promised';

import { clearDataStore } from '../../bootstrap.mjs';

chai.use(chaiAsPromised);

describe('delete shortcut controller action', function () {
  let accessToken;
  let accessToken2;

  beforeEach(async function () {
    const userResponse1 = await fastify.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        name: 'Siddhant Kumar Patel',
        username: 'oslash',
        password: 'hi-there-abcd'
      }
    });
    const userReponse2 = await fastify.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        name: 'Sergio Busquets',
        username: 'busquets',
        password: 'hi-there-abcd'
      }
    });

    accessToken = userResponse1.json().token;
    accessToken2 = userReponse2.json().token;
  });

  afterEach(async function() {
    await clearDataStore(fastify.elastic, fastify.sequelize);
  });

  it('should relay unauthorized error if access token is not provided', async function () {
    const res = await fastify.inject({
      method: 'DELETE',
      url: '/shortcuts/abcd'
    });

    expect(res.statusCode).to.equal(401);
  });

  it('should relay unauthorized error if incorrect access token is provided', async function () {
    const res = await fastify.inject({
      method: 'DELETE',
      url: '/shortcuts/abcd',
      headers: {
        authorization: 'Bearer: abcdefgh'
      }
    });

    expect(res.statusCode).to.equal(401);
  });

  it('should relay forbidden error while trying to delete a non-existent shortcut', async function () {
    const res = await fastify.inject({
      method: 'DELETE',
      url: '/shortcuts/abcd',
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });

    expect(res.statusCode).to.equal(403);
  });

  it('should relay forbidden error when one user tries to delete shortcut of another user', async function () {
    const createShortcutPaylaod = {
      shortlink: 'standup',
      description: 'Google meet standup link',
      url: 'https://meet.google.com/abc-123-xyz',
      tags: [
        "standup",
        "meeting",
        "google"
      ]
    };
    const createShortcutResponse = await fastify.inject({
      url: '/shortcuts',
      method: 'POST',
      payload: createShortcutPaylaod,
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });
    const shortcutId = createShortcutResponse.json().id;

    await fastify.elastic.indices.refresh({
      index: '_all'
    });

    const deleteShortcutResponse = await fastify.inject({
      method: 'DELETE',
      url: `/shortcuts/${shortcutId}`,
      headers: {
        authorization: `Bearer ${accessToken2}`
      }
    });

    expect(deleteShortcutResponse.statusCode).to.equal(403);
  });

  it('should successfully delete the shortcut of a user', async function () {
    const createShortcutPaylaod = {
      shortlink: 'standup',
      description: 'Google meet standup link',
      url: 'https://meet.google.com/abc-123-xyz',
      tags: [
        "standup",
        "meeting",
        "google"
      ]
    };
    const createShortcutResponse = await fastify.inject({
      url: '/shortcuts',
      method: 'POST',
      payload: createShortcutPaylaod,
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });
    const shortcutId = createShortcutResponse.json().id;

    await fastify.elastic.indices.refresh({
      index: '_all'
    });

    const deleteShortcutResponse = await fastify.inject({
      method: 'DELETE',
      url: `/shortcuts/${shortcutId}`,
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });

    expect(deleteShortcutResponse.statusCode).to.equal(200);
    expect(deleteShortcutResponse.json()).to.have.property('id', shortcutId);

    await fastify.elastic.indices.refresh({
      index: '_all'
    });

    await expect(fastify.elastic.get({
      index: 'shortcuts',
      id: shortcutId
    })).to.be.rejectedWith({
      _index: 'shortcuts',
      _type: '_doc',
      _id: shortcutId,
      found: false
    });
  });
});
