import { expect } from 'chai';

import { clearDataStore } from '../../bootstrap.mjs';


describe('user signout controller action', function () {
  let accessToken;

  beforeEach(async function () {
    const res = await fastify.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        name: 'Siddhant Kumar Patel',
        username: 'oslash',
        password: 'hi-there-abcd'
      }
    });

    accessToken = res.json().token;
  });

  afterEach(async function () {
    await clearDataStore(fastify.elastic, fastify.sequelize);
  });

  it('should signout an authenticated user', async function () {
    const res = await fastify.inject({
      method: 'POST',
      url: '/signout',
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });

    expect(res.statusCode).to.equal(205);
  });

  it('should relay unauthorized error while trying to signout using an old access token', async function () {
    const firstSignoutRes = await fastify.inject({
      method: 'POST',
      url: '/signout',
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });

    expect(firstSignoutRes.statusCode).to.equal(205);

    const secondSignoutRes = await fastify.inject({
      method: 'POST',
      url: '/signout',
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });

    expect(secondSignoutRes.statusCode).to.equal(401);
  });

  it('should relay unauthorized error when trying to create shortcut using an old access token', async function () {
    await fastify.inject({
      method: 'POST',
      url: '/signout',
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });

    const res = await fastify.inject({
      url: '/shortcuts',
      method: 'POST',
      payload: {
        shortlink: 'standup',
        description: 'Google meet standup link',
        url: 'https://meet.google.com/abc-123-xyz',
        tags: [
          "standup",
          "meeting",
          "google"
        ]
      },
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });

    expect(res.statusCode).to.equal(401);
  });

  it('should relay unauthorized error when trying to delete shortcut using an old access token', async function () {
    await fastify.inject({
      method: 'POST',
      url: '/signout',
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });

    const res = await fastify.inject({
      url: '/shortcuts/abcd',
      method: 'DELETE',
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });

    expect(res.statusCode).to.equal(401);
  });

  it('should relay unauthorized error when trying to find shortcuts using an old access token', async function () {
    await fastify.inject({
      method: 'POST',
      url: '/signout',
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });

    const res = await fastify.inject({
      method: 'GET',
      url: '/shortcuts',
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    });

    expect(res.statusCode).to.equal(401);
  });
});
