import { expect } from 'chai';

import { clearDataStore } from '../../bootstrap.mjs';

describe('user signup controller action', function () {
  afterEach(async function () {
    await clearDataStore(fastify.elastic, fastify.sequelize);
  });

  it('should relay bad request error if incorrect paylod is sent', async function () {
    const payload = {
      name: 'Siddhant Kumar Patel',
      password: '234'
    };
    const res = await fastify.inject({
      method: 'POST',
      url: '/signup',
      payload: payload
    });

    expect(res.statusCode).to.equal(400);
  });

  it('should create user', async function () {
    const payload = {
      name: 'Siddhant Kumar Patel',
      username: 'oslash',
      password: 'hi-there-abcd'
    };
    const res = await fastify.inject({
      method: 'POST',
      url: '/signup',
      payload: payload
    });
    const response = res.json();

    expect(response).to.have.property('id', 1);
    expect(response.token).to.be.ok;
  });

  it('should relay error if a new user is created with an already existing username', async function () {
    const payload = {
      name: 'Siddhant Kumar Patel',
      username: 'oslash',
      password: 'hi-there-abcd'
    };
    const createFirstUserResponse = await fastify.inject({
      method: 'POST',
      url: '/signup',
      payload: payload
    });
    const createSecondUserResponse = await fastify.inject({
      method: 'POST',
      url: '/signup',
      payload: payload
    });

    expect(createFirstUserResponse.statusCode).to.equal(201);
    expect(createSecondUserResponse.statusCode).to.equal(409);
  });
});
