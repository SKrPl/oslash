import { expect } from 'chai';

import { clearDataStore } from '../../bootstrap.mjs';

describe('user signin controller action', function () {
  afterEach(async function () {
    await clearDataStore(fastify.elastic, fastify.sequelize);
  });

  it('should relay error if an unregistered user tries to signin', async function () {
    const res = await fastify.inject({
      method: 'POST',
      url: '/signin',
      payload: {
        username: 'hello-world',
        password: 'hi there whats up'
      }
    });

    expect(res.statusCode).to.equal(401);
  });

  it('should successfully signin a registered user and send jwt token in response', async function () {
    const userCredentials = {
      username: 'busquets',
      password: 'ILoveBarca'
    };

    await fastify.inject({
      method: 'POST',
      url: '/signup',
      payload: { ...userCredentials, name: 'Sergio Busquets' } 
    });

    const response = await fastify.inject({
      method: 'POST',
      url: '/signin',
      payload: userCredentials
    });

    const res = response.json();

    expect(response.statusCode).to.equal(200);
    expect(Object.keys(res)).to.have.members([
      'id',
      'token',
    ]);
  });

  it('should relay unauthorized error when incorrect credentials are used for signin', async function () {
    const userCredentials = {
      username: 'busquets',
      password: 'ILoveBarca'
    };

    await fastify.inject({
      method: 'POST',
      url: '/signup',
      payload: { ...userCredentials, name: 'Sergio Busquets' } 
    });

    const response = await fastify.inject({
      method: 'POST',
      url: '/signin',
      payload: { ...userCredentials, password: 'i_love_barca' }
    });

    expect(response.statusCode).to.equal(401);
  });
});
