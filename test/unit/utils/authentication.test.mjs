import { expect } from 'chai';
import JWT from 'jsonwebtoken';

import { sign, verify } from '../../../api/utils/authentication.mjs';

const { decode, JsonWebTokenError } = JWT;

describe('authentication', function () {
  it('should return a signed jwt', function () {
    const payload = { id: 1, username: 'oslash' };
    const secret = 'hello world';
    const jwt = sign(payload, secret, '1h');
    const decodedToken = decode(jwt, { json: true });

    expect(Object.keys(decodedToken)).to.have.members([
      'id',
      'username',
      'exp',
      'iat'
    ]);
    expect(decodedToken.id).to.equal(1);
    expect(decodedToken.username).to.equal('oslash');
  });

  it('should verify a signed token', function () {
    const payload = { id: 1, username: 'oslash' };
    const secret = 'hello world';
    const jwt = sign(payload, secret, '1h');
    const verifiedToken = verify(jwt, secret);

    expect(Object.keys(verifiedToken)).to.have.members([
      'id',
      'username',
      'exp',
      'iat'
    ]);
    expect(verifiedToken.id).to.equal(1);
    expect(verifiedToken.username).to.equal('oslash');
  });

  it('should throw error if there is a secret mismatch while verifying', function () {
    const payload = { id: 1, username: 'oslash' };
    const secret = 'hello world';
    const jwt = sign(payload, secret, '1h');

    expect(function () {
      verify(jwt, secret + 'abcd')
    }).throw(JsonWebTokenError);
  });
});
