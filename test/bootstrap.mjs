import { readFileSync } from 'fs';

import Fastify from 'fastify';
import { Client } from '@elastic/elasticsearch';

import { registerPlugins, pluginConfigs } from '../helper.mjs';


const fastify = Fastify();
const shortcutMappig = JSON.parse(readFileSync('./api/models/mappings/shortcut.json', { encoding: 'utf-8' }));

/**
 * Delete all the documents present in non-default indices.
 * 
 * @param {Client} elastic 
 */
export async function clearDataStore (elastic, sequelize) {
  // perform all the pending recent operations on all the indices
  await elastic.indices.refresh({
    index: '_all'
  });

  const { body: shortcutIndexExists } = await fastify.elastic.indices.exists({
    index: 'shortcuts'
  });

  if (shortcutIndexExists) {
    await elastic.indices.delete({
      index: 'shortcuts'
    });
  }

  await fastify.elastic.indices.create({
    index: 'shortcuts',
    body: {
      mappings: shortcutMappig
    }
  });

  sequelize && await sequelize.sync({ force: true });
}

export const mochaGlobalSetup = async () => {
  registerPlugins(fastify, pluginConfigs);
  await fastify.ready();

  global.fastify = fastify;

  await fastify.elastic.ping();
  await clearDataStore(fastify.elastic, fastify.sequelize);
};

export const mochaGlobalTeardown = async () => {
  await fastify.close();
};
